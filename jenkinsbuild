#!/bin/sh

set -o monitor
set -o nounset
set -o noglob

main () {
    #
    # CONFIGURATIONS START
    #
    local JENKINS_URL=${JENKINS_URL-}
    local JENKINS_USER=${JENKINS_USER-}
    local JENKINS_TOKEN=${JENKINS_TOKEN-}
    local JENKINS_JOB_NAME=${JENKINS_JOB_NAME-}
    local JENKINS_SRC_DIR=${JENKINS_SRC_DIR-'.'}
    #
    # CONFIGURATIONS END
    #
    local daemon_only= param= data= commitish= jobswitch=
    local with_param=${JENKINS_WITH_PARAMETER:-0}
    local no_with_param=0
    local version="0.11.1"
    local lupdate="2023-07-28"
    while test $# -ge 1
    do
        case $1 in
        -d|--daemon-only)
            daemon_only=1
            shift
            ;;
        -k|--kill-daemon)
            KillDaemon
            exit
            ;;
        -p|--parameter)
            test $# -lt 2 && err "switch $1 requires a value"
            param=$(printf "%s%s\t" "$param" "$2")
            shift 2
            ;;
        -w|--with-parameter)
            with_param=1
            no_with_param=0
            shift
            ;;
        -n|--no-with-parameter)
            no_with_param=1
            with_param=0
            shift
            ;;
        -j|--job|--job-name)
            test $# -lt 2 && err "switch $1 requires a value"
            jobswitch=1
            JENKINS_JOB_NAME=$2
            shift 2
            ;;
        -h|-\?|--help)
            ShowHelp
            printf "\n"
            ShowVersion
            exit 0
            ;;
        --version)
            ShowVersion
            exit 0
            ;;
        -*)
            err "invalid switch \`$1'"
            ;;
        *)
            test "$commitish" && err "invalid argument: $1"
            commitish=$1
            shift 1
            ;;
        esac
    done
    GitDaemon || return
    FormatParam
    ReadJobNameFromGitConfig
    CallApi
}

ReadJobNameFromGitConfig () {
    local jobname
    test "${jobswitch-}" && return
    jobname=$(git config --local --get jenkinsbuild.job) || return
    JENKINS_JOB_NAME=$jobname
}

KillDaemon () {
    ps -afW | grep -q 'git-daemon\.exe' || return 0
    taskkill //f //im git-daemon.exe 1>/dev/null
}

GitDaemon () {
    local toplevel
    KillDaemon || return
    (
        cd "$JENKINS_SRC_DIR" || return
        toplevel=$(git rev-parse --show-toplevel) || return
        (
            cd "$toplevel" || return
            git daemon --export-all --enable=upload-archive \
                --base-path="$JENKINS_SRC_DIR" &
        )
    ) || return
    test "$daemon_only" && exit
    return 0
}

FormatParam () {
    local IFS=$(printf '\t')
    test ! "$param" && return
    test "$no_with_param" -eq 0 ||
        err "-p or --parameter conflicts with -n or --no-with-parameter"
    with_param=1
    for p in $param
    do
        data="$data --data $p"
    done
}

CallApi () {
    local api="build" commitish_parsed
    test "$JENKINS_USER" &&
        test "$JENKINS_TOKEN" &&
        test "$JENKINS_URL" &&
        test "$JENKINS_JOB_NAME" ||
        err "JENKINS_USER, JENKINS_TOKEN, JENKINS_URL, or JENKINS_JOB_NAME undefined" \
        "Try 'jenkinsbuild --help' for more information."
    test "$with_param" -eq 0 || api="buildWithParameters"
    commitish_parsed=$(git rev-parse --verify "${commitish:-HEAD}") ||
        err "failed to parse commitish \`$commitish'"
    curl --silent --show-error --request POST \
        --user "$JENKINS_USER:$JENKINS_TOKEN" \
        "$JENKINS_URL/job/$JENKINS_JOB_NAME/$api" \
        --data commitish="$commitish_parsed" \
        $data
}

ShowHelp () {
cat <<EOF
usage: jenkinsbuild [<option> ...] [<commitish>]

Build <commitish> in a specific Jenkins job. <commitish>, if not specified,
is HEAD, i.e. current checked out commit.

options:
    -j, --job JOB               specify Jenkins job name to \`JOB' for this
                                 build only
    -d, --daemon-only           start git-daemon only
    -k, --kill-daemon           kill git-daemon and exit
    -w, --with-parameter        for Jenkins jobs having parameters
    -n, --no-with-parameter     for Jenkins jobs not having parameters; this
                                 is the default, but you can set environment
                                 variable JENKINS_WITH_PARAMETER to 1 to set
                                 --with-parameter as the default
    -p, --parameter             specify build parameters; this implies
                                 --with-parameter; this option can be used
                                 multiple times to specify more than one
                                 parameters

Following steps are required to be done before first use.
    1. In the Jenkins job, set git branch to build to \`jenkinsbuild'.
    2. Set following environment variables.
        JENKINS_URL: Jenkins URL such as \`http://example.com:8080'
        JENKINS_USER: Jenkins user name
        JENKINS_TOKEN: API token of JENKINS_USER
        JENKINS_JOB_NAME: Name of job to build when --job is not specified
        JENKINS_SRC_DIR: Path to the source code directory

Git local config \`jenkinsbuild.job', if defined, overrides environment variable
JENKINS_JOB_NAME, but would be overridden by switch -j/--job, if specified.
EOF
}

ShowVersion () {
cat <<EOF
jenkinsbuild $version : $lupdate : Liu, Zhao-hui <liuzhaohui@ieisystem.com>
https://gitlab.com/lzhh/jenkinsbuild
EOF
}

err () {
    >&2 printf "error: %s\n" "$1"
    shift
    test $# -gt 0 && >&2 printf "%s\n" "$@"
    exit 1
}

main "$@"
